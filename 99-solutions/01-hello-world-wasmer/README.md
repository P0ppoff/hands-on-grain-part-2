# Wasmer / WASI and TinyGo

## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module hello

go 1.17
```

## main.go

```golang
package main

import (
	"fmt"
	"os"
)

func main() {

	fmt.Println("Hello World from Go")
	args := os.Args
	argsWithoutCaller := os.Args[1:]

	fmt.Println(args)
	fmt.Println(argsWithoutCaller)

}
```

## Build (with tinygo)

```bash
tinygo build -o hello.wasm -target wasi .
# check the size
ls -lh *.wasm
```

## Run

```bash
wasmer hello.wasm
wasmer hello.wasm bob morane
```